import React, { useState } from "react";
import "./index.css";

interface SquareProps {
    value: string;
    onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

export const Square: React.FunctionComponent<SquareProps> = (props: SquareProps) => {

    let [value, setValue] = useState<any>(null);

    // let clickSquare = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    //     setValue('X');
    //     event.preventDefault();
    // };

    return (
        <button className="square" onClick={props.onClick}>
            {props.value}
        </button >
    );
};
